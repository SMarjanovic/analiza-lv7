﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KrizicKruzic
{
    public partial class Form1 : Form
    {
        Graphics g00, g01, g02, g10, g11, g12, g20, g21, g22;
        int[][] polje = new int[3][];
        
        int turn = 1;
        int score1 = 0;
        int score2 = 0;

        private void cleargame()
        {
            g00.Clear(Color.LightGray);
            g01.Clear(Color.LightGray);
            g02.Clear(Color.LightGray);
            g10.Clear(Color.LightGray);
            g11.Clear(Color.LightGray);
            g12.Clear(Color.LightGray);
            g20.Clear(Color.LightGray);
            g21.Clear(Color.LightGray);
            g22.Clear(Color.LightGray);
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                    polje[i][j] = 0;
            }
            turn = 1;
        }

        private void endtest()
        {
            if((polje[0][0] == polje[0][1]) && (polje[0][1] == polje[0][2]) && (polje[0][0] != 0))
            {
                if (polje[0][0] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ((polje[1][0] == polje[1][1]) && (polje[1][1] == polje[1][2]) && (polje[1][0] != 0))
            {
                if (polje[1][0] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ((polje[2][0] == polje[2][1]) && (polje[2][1] == polje[2][2]) && (polje[2][0] != 0))
            {
                if (polje[2][0] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ((polje[0][0] == polje[1][0]) && (polje[1][0] == polje[2][0]) && (polje[0][0] != 0))
            {
                if (polje[0][0] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ((polje[0][1] == polje[1][1]) && (polje[1][1] == polje[2][1]) && (polje[0][1] != 0))
            {
                if (polje[0][1] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ((polje[0][2] == polje[1][2]) && (polje[1][2] == polje[2][2]) && (polje[0][2] != 0))
            {
                if (polje[0][2] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ((polje[0][0] == polje[1][1]) && (polje[1][1] == polje[2][2]) && (polje[0][0] != 0))
            {
                if (polje[0][0] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ((polje[2][0] == polje[1][1]) && (polje[1][1] == polje[0][2]) && (polje[0][2] != 0))
            {
                if (polje[0][2] < 2)
                {
                    MessageBox.Show("Pobijedio je " + textBox1.Text);
                    score1++;
                    score01.Text = score1.ToString();
                }
                else
                {
                    MessageBox.Show("Pobijedio je " + textBox2.Text);
                    score2++;
                    score02.Text = score2.ToString();
                }
                cleargame();
            }
            else if ( (polje[0][0]!=0) && (polje[0][1] != 0) && (polje[0][2] != 0) && (polje[1][0] != 0) && (polje[1][1] != 0) && (polje[1][2] != 0) && (polje[2][0] != 0) && (polje[2][1] != 0) && (polje[2][2] != 0))
            {
                MessageBox.Show("Nerjeseno!");
                cleargame();
            }

        }

        private void pb20_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[2][0] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[2][0] = 1;
                    Cross c = new Cross();
                    c.draw(g20, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[2][0] = 2;
                    Circle c = new Circle();
                    c.draw(g20, pens[0]);
                }
                endtest();

            }
        }

        private void pb21_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[2][1] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[2][1] = 1;
                    Cross c = new Cross();
                    c.draw(g21, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[2][1] = 2;
                    Circle c = new Circle();
                    c.draw(g21, pens[0]);
                }
                endtest();
            }
        }

        private void pb22_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[2][2] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[2][2] = 1;
                    Cross c = new Cross();
                    c.draw(g22, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[2][2] = 2;
                    Circle c = new Circle();
                    c.draw(g22, pens[0]);
                }
                endtest();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (turn == 2)
                textBox3.Text = textBox2.Text + " na potezu";
                
            tbscore2.Text = textBox2.Text + " rezultat:";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (turn == 1)
                textBox3.Text = textBox1.Text + " na potezu";
                tbscore1.Text = textBox1.Text + " rezultat:";
        }

        private void pb12_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[1][2] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[1][2] = 1;
                    Cross c = new Cross();
                    c.draw(g12, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[1][2] = 2;
                    Circle c = new Circle();
                    c.draw(g12, pens[0]);
                }
                endtest();
            }
        }

        private void pb11_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[1][1] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[1][1] = 1;
                    Cross c = new Cross();
                    c.draw(g11, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[1][1] = 2;
                    Circle c = new Circle();
                    c.draw(g11, pens[0]);
                }
                endtest();
            }
        }

        private void pb10_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[1][0] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[1][0] = 1;
                    Cross c = new Cross();
                    c.draw(g10, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[1][0] = 2;
                    Circle c = new Circle();
                    c.draw(g10, pens[0]);
                }
                endtest();
            }
        }

        private void pb02_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[0][2] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[0][2] = 1;
                    Cross c = new Cross();
                    c.draw(g02, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[0][2] = 2;
                    Circle c = new Circle();
                    c.draw(g02, pens[0]);
                }
                endtest();
            }
        }

        private void pb01_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[0][1] == 0)
            {
                if (turn == 1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[0][1] = 1;
                    Cross c = new Cross();
                    c.draw(g01, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu";
                    polje[0][1] = 2;
                    Circle c = new Circle();
                    c.draw(g01, pens[0]);
                }
                endtest();
            }
        }

        private void pb00_MouseUp(object sender, MouseEventArgs e)
        {
            if (polje[0][0] == 0)
            {
                if (turn ==1)
                {
                    turn = 2;
                    textBox3.Text = textBox2.Text + " na potezu";
                    polje[0][0] = 1;
                    Cross c = new Cross();
                    c.draw(g00, pens[1]);
                }
                else
                {
                    turn = 1;
                    textBox3.Text = textBox1.Text + " na potezu"; 
                    polje[0][0] = 2;
                    Circle c = new Circle();
                    c.draw(g00, pens[0]);
                }
                endtest();
            }
        }

        List<Pen> pens = new List<Pen>();
        public Form1()
        {
            InitializeComponent();
            pens.Add(new Pen(Color.Red, 4F));
            pens.Add(new Pen(Color.Blue, 4F));
            g00 = pb00.CreateGraphics();
            g01 = pb01.CreateGraphics();
            g02 = pb02.CreateGraphics();
            g10 = pb10.CreateGraphics();
            g11 = pb11.CreateGraphics();
            g12 = pb12.CreateGraphics();
            g20 = pb20.CreateGraphics();
            g21 = pb21.CreateGraphics();
            g22 = pb22.CreateGraphics();
            textBox3.Text=textBox1.Text + " na potezu";
            score01.Text = "0";
            score02.Text = "0";
            tbscore1.Text = textBox1.Text + " rezultat:";
            tbscore2.Text = textBox2.Text + " rezultat:";
            polje[0] = new int[3];
            polje[1] = new int[3];
            polje[2] = new int[3];
            for  (int i = 0; i<3; i++)
            {
                for (int j = 0; j < 3; j++)
                    polje[i][j] = 0;
            }
        }

    }
    class Circle
    {
        int r;
        public Circle()
        {
            r = 70;
        }
        public void draw(Graphics g, Pen p)
        {
            g.DrawEllipse(p, 15, 15, r, r);
        }
    }
    class Cross
    {
        public void draw(Graphics g, Pen p)
        {
            g.DrawLine(p, 10, 10, 90, 90);
            g.DrawLine(p, 90, 10, 10, 90);
        }
    }
  
}
