﻿namespace KrizicKruzic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb00 = new System.Windows.Forms.PictureBox();
            this.pb01 = new System.Windows.Forms.PictureBox();
            this.pb02 = new System.Windows.Forms.PictureBox();
            this.pb10 = new System.Windows.Forms.PictureBox();
            this.pb11 = new System.Windows.Forms.PictureBox();
            this.pb12 = new System.Windows.Forms.PictureBox();
            this.pb20 = new System.Windows.Forms.PictureBox();
            this.pb21 = new System.Windows.Forms.PictureBox();
            this.pb22 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Igrac1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tbscore1 = new System.Windows.Forms.TextBox();
            this.tbscore2 = new System.Windows.Forms.TextBox();
            this.score01 = new System.Windows.Forms.TextBox();
            this.score02 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb22)).BeginInit();
            this.SuspendLayout();
            // 
            // pb00
            // 
            this.pb00.BackColor = System.Drawing.Color.LightGray;
            this.pb00.Location = new System.Drawing.Point(46, 92);
            this.pb00.Name = "pb00";
            this.pb00.Size = new System.Drawing.Size(100, 100);
            this.pb00.TabIndex = 0;
            this.pb00.TabStop = false;
            this.pb00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb00_MouseUp);
            // 
            // pb01
            // 
            this.pb01.BackColor = System.Drawing.Color.LightGray;
            this.pb01.Location = new System.Drawing.Point(152, 92);
            this.pb01.Name = "pb01";
            this.pb01.Size = new System.Drawing.Size(100, 100);
            this.pb01.TabIndex = 1;
            this.pb01.TabStop = false;
            this.pb01.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb01_MouseUp);
            // 
            // pb02
            // 
            this.pb02.BackColor = System.Drawing.Color.LightGray;
            this.pb02.Location = new System.Drawing.Point(258, 92);
            this.pb02.Name = "pb02";
            this.pb02.Size = new System.Drawing.Size(100, 100);
            this.pb02.TabIndex = 2;
            this.pb02.TabStop = false;
            this.pb02.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb02_MouseUp);
            // 
            // pb10
            // 
            this.pb10.BackColor = System.Drawing.Color.LightGray;
            this.pb10.Location = new System.Drawing.Point(46, 198);
            this.pb10.Name = "pb10";
            this.pb10.Size = new System.Drawing.Size(100, 100);
            this.pb10.TabIndex = 3;
            this.pb10.TabStop = false;
            this.pb10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb10_MouseUp);
            // 
            // pb11
            // 
            this.pb11.BackColor = System.Drawing.Color.LightGray;
            this.pb11.Location = new System.Drawing.Point(152, 198);
            this.pb11.Name = "pb11";
            this.pb11.Size = new System.Drawing.Size(100, 100);
            this.pb11.TabIndex = 4;
            this.pb11.TabStop = false;
            this.pb11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb11_MouseUp);
            // 
            // pb12
            // 
            this.pb12.BackColor = System.Drawing.Color.LightGray;
            this.pb12.Location = new System.Drawing.Point(258, 198);
            this.pb12.Name = "pb12";
            this.pb12.Size = new System.Drawing.Size(100, 100);
            this.pb12.TabIndex = 5;
            this.pb12.TabStop = false;
            this.pb12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb12_MouseUp);
            // 
            // pb20
            // 
            this.pb20.BackColor = System.Drawing.Color.LightGray;
            this.pb20.Location = new System.Drawing.Point(46, 304);
            this.pb20.Name = "pb20";
            this.pb20.Size = new System.Drawing.Size(100, 100);
            this.pb20.TabIndex = 6;
            this.pb20.TabStop = false;
            this.pb20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb20_MouseUp);
            // 
            // pb21
            // 
            this.pb21.BackColor = System.Drawing.Color.LightGray;
            this.pb21.Location = new System.Drawing.Point(152, 304);
            this.pb21.Name = "pb21";
            this.pb21.Size = new System.Drawing.Size(100, 100);
            this.pb21.TabIndex = 7;
            this.pb21.TabStop = false;
            this.pb21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb21_MouseUp);
            // 
            // pb22
            // 
            this.pb22.BackColor = System.Drawing.Color.LightGray;
            this.pb22.Location = new System.Drawing.Point(258, 304);
            this.pb22.Name = "pb22";
            this.pb22.Size = new System.Drawing.Size(100, 100);
            this.pb22.TabIndex = 8;
            this.pb22.TabStop = false;
            this.pb22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb22_MouseUp);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(511, 81);
            this.textBox1.MaxLength = 20;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(220, 20);
            this.textBox1.TabIndex = 9;
            this.textBox1.Text = "Prvi igrac";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(511, 107);
            this.textBox2.MaxLength = 20;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(220, 20);
            this.textBox2.TabIndex = 10;
            this.textBox2.Text = "Drugi igrac";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // Igrac1
            // 
            this.Igrac1.AutoSize = true;
            this.Igrac1.Location = new System.Drawing.Point(462, 84);
            this.Igrac1.Name = "Igrac1";
            this.Igrac1.Size = new System.Drawing.Size(43, 13);
            this.Igrac1.TabIndex = 11;
            this.Igrac1.Text = "Igrac 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(462, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Igrac 2:";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox3.Location = new System.Drawing.Point(407, 238);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(368, 31);
            this.textBox3.TabIndex = 13;
            // 
            // tbscore1
            // 
            this.tbscore1.Location = new System.Drawing.Point(441, 304);
            this.tbscore1.Name = "tbscore1";
            this.tbscore1.Size = new System.Drawing.Size(168, 20);
            this.tbscore1.TabIndex = 14;
            // 
            // tbscore2
            // 
            this.tbscore2.Location = new System.Drawing.Point(441, 339);
            this.tbscore2.Name = "tbscore2";
            this.tbscore2.Size = new System.Drawing.Size(168, 20);
            this.tbscore2.TabIndex = 15;
            // 
            // score01
            // 
            this.score01.Location = new System.Drawing.Point(615, 304);
            this.score01.Name = "score01";
            this.score01.Size = new System.Drawing.Size(100, 20);
            this.score01.TabIndex = 16;
            // 
            // score02
            // 
            this.score02.Location = new System.Drawing.Point(615, 339);
            this.score02.Name = "score02";
            this.score02.Size = new System.Drawing.Size(100, 20);
            this.score02.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.score02);
            this.Controls.Add(this.score01);
            this.Controls.Add(this.tbscore2);
            this.Controls.Add(this.tbscore1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Igrac1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pb22);
            this.Controls.Add(this.pb21);
            this.Controls.Add(this.pb20);
            this.Controls.Add(this.pb12);
            this.Controls.Add(this.pb11);
            this.Controls.Add(this.pb10);
            this.Controls.Add(this.pb02);
            this.Controls.Add(this.pb01);
            this.Controls.Add(this.pb00);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb22)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb00;
        private System.Windows.Forms.PictureBox pb01;
        private System.Windows.Forms.PictureBox pb02;
        private System.Windows.Forms.PictureBox pb10;
        private System.Windows.Forms.PictureBox pb11;
        private System.Windows.Forms.PictureBox pb12;
        private System.Windows.Forms.PictureBox pb20;
        private System.Windows.Forms.PictureBox pb21;
        private System.Windows.Forms.PictureBox pb22;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label Igrac1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox tbscore1;
        private System.Windows.Forms.TextBox tbscore2;
        private System.Windows.Forms.TextBox score01;
        private System.Windows.Forms.TextBox score02;
    }
}

